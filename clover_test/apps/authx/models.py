'''
author: 鲁成
create_tiem: 2017-09-13
'''

from django.db import models
from django.contrib.auth.models import User

class UserRole(models.Model):
    SEX_TYPE = (
        ('man', u'男'),
        ('woman', u'女'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='userrole')
    username_cn = models.CharField(max_length=50, null=True, blank=True) # 用户中文名
    gender = models.CharField(max_length=20, choices=SEX_TYPE, null=False, blank=True, default='man') # 性别

    def __unicode__(self):
        return self.user.username