#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-09-13
'''

from rest_framework import serializers

from django.contrib.auth.models import User
from apps.authx.models import UserRole
from apps.article.models import Article


class UserListSerializer(serializers.ModelSerializer):
    '''
    用户信息serializer
    '''
    username_cn = serializers.SerializerMethodField()
    gender = serializers.SerializerMethodField()
    articles = serializers.PrimaryKeyRelatedField(many=True, queryset=Article.objects.all())

    class Meta:
        model = User
        fields = ['username', 'username_cn', 'gender', 'articles']

    def get_username_cn(self, obj):
        try:
            return obj.userrole.username_cn
        except:
            return ''

    def get_gender(self, obj):
        try:
            return obj.userrole.get_gender_display()
        except:
            return ''
