#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-09-13
'''

from django.conf.urls import url

from apps.authx.views import UserList, LoginUser, logout_user

urlpatterns = [
    url(r'^users/$', UserList.as_view(), name='users'),
    url(r'^login/$', LoginUser.as_view(), name='loginurl'),
    url(r'^logout/$', logout_user, name='logouturl'),
]