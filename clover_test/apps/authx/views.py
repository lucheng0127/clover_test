#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-09-13
'''

from django.shortcuts import render

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from apps.authx.models import UserRole
from apps.authx.user_list_serializer import UserListSerializer

from common_utils.validate_str import validate_str


class UserList(APIView):
    '''
    GET获取用户列表，POST新增用户
    '''
    def get(self, request, format=None):
        user_obj = User.objects.all()
        user_serializer = UserListSerializer(user_obj, many=True)
        return Response(user_serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        post_parmas = {
            'username': request.POST.get('username'),
            'password': request.POST.get('password'),
            'username_cn': request.POST.get('username_cn'),
            'gender': request.POST.get('gender', 'man'),
        }

        # 校验特殊字符
        for k, v in post_parmas.items():
            if not validate_str(v):
                return Response({'msg': '含有特殊字符！', 'errorCode': '400'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            cache_user = User.objects.create_user(username=post_parmas['username'],email='',password=post_parmas['password'])
            print(cache_user.username)
            save_parmas = {
                'user': cache_user,
                'username_cn': post_parmas['username_cn'],
                'gender': post_parmas['gender'],
            }
            print(save_parmas)
            cache_user_role = UserRole(**save_parmas)
            cache_user_role.save()
            # 记录log
            return Response({'msg': '创建用户成功！'}, status=status.HTTP_200_OK)
        except:
            # 记录log
            return Response({'msg': '创建用户失败！', 'errorCode': '400'}, status=status.HTTP_400_BAD_REQUEST)


class LoginUser(APIView):
    '''
    登录用户
    '''
    def get(self, request, format=None):
        return render(request, 'user/login.html')

    def post(self, request, format=None):
        post_parmas = request.POST
        username = post_parmas['username']
        password = post_parmas['password']
        if not User.objects.filter(username=username).exists():
            return Response({'msg': '用户不存在', 'errorCode': '400'}, status=status.HTTP_400_BAD_REQUEST)
        user = authenticate(username=username, password=password)
        if user is None:
            return Response({'msg': '密码错误', 'errorCode': '401'}, status=status.HTTP_401_UNAUTHORIZED)
        login(request, user)
        # 记录log
        return Response({'status': 'success', 'msg': '登入成功'})


@login_required
def logout_user(request):
    '''
    退出当前用户
    '''
    logout(request)
    return render(request, 'user/login.html')
