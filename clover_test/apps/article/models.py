'''
author: 鲁成
create_tiem: 2017-09-13
'''

from django.db import models
from django.contrib.auth.models import User

from common_utils.validate_str import validate_str

class Article(models.Model):
    '''
    文章
    '''
    title = models.CharField(max_length=64, null=True, blank=True) # 文章标题
    body = models.TextField(null=True, blank=True) # 文章内容
    created_time = models.DateTimeField(auto_now_add=True) # 创建时间
    mod_time = models.DateTimeField(auto_now=True) # 上一次编辑时间
    author = models.ForeignKey(User, null=True, related_name='articles', on_delete=models.SET_NULL) # 作者
    stars_record = models.IntegerField(default=0) # 被点赞次数记录

    def clean(self):
        if not (validate_str(self.title) and validate_str(self.body)):
            raise ValueError('不能含有特殊字符！')

    def __unicode__(self):
        return self.title

    def _get_stars_num(self):
        return self.stars.count()

    stars_num = property(_get_stars_num) # 被点赞次数


class StarsStatistics(models.Model):
    mark_by = models.ForeignKey(User, related_name='marks', db_column='username') # 点赞的人
    star_to = models.ForeignKey(Article, related_name='stars', on_delete=models.CASCADE) # 被点赞的文章
