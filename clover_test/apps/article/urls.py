#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-09-13
last_mod_time : 2017-09-14
'''

from django.conf.urls import url

from django.contrib.auth.decorators import login_required

# from apps.article.views.article_views import AllArticle, ArticleList, ModArticle, ArticleDetail, ArticlesList
from apps.article.views.article_views import ArticleDetail, ArticlesList
from apps.article.views.mark_and_star_views import MarkArticle

# login url 为： /api-auth/login/

urlpatterns = [
    # url(r'^all_articles/$', login_required(AllArticle.as_view()), name='articles'), # get 查看所有人的文章，支持作者和文章名称筛选 post 删除文章
    # url(r'^my_article/$', login_required(ArticleList.as_view()), name='my_article'), # get 查看自己的文章 post 发布文章
    # url(r'^atricle_details/$', login_required(ModArticle.as_view()), name='atricle_details'), # get 查看某一篇文章 post 修改谋篇文章内容
    # url(r'^stars/$', login_required(MarkArticle.as_view()), name='stars'), # post请求为文章点赞
    # 上面的接口不符合restful规范
    # 符合restful的接口如下
    url(r'^articles/$', login_required(ArticlesList.as_view()), name='articles'),  # get 查看所有人的文章，支持作者和文章名称筛选 post 删除文章
    url(r'^article/(?P<pk>[0-9]+)/$', login_required(ArticleDetail.as_view()), name='article'),  # restful 风格的查，删，改接口
]
