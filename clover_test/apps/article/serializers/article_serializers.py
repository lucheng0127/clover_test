#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-09-13
last_mod_time : 2017-09-14
'''

from rest_framework import serializers

from apps.article.models import Article, StarsStatistics

class ArticleListSerializer(serializers.ModelSerializer):
    '''
    返回文章信息
    '''
    star_dis_num = serializers.SerializerMethodField()
    star_people_list = serializers.SerializerMethodField()
    author = serializers.ReadOnlyField(source='author.userrole.username_cn')

    class Meta:
        model = Article
        fields = ['title', 'body', 'author', 'star_dis_num', 'star_people_list']

    def validate(self, attrs):
        '''
        检查参数是否合法
        :param attrs:
        :return:
        '''
        instance = Article(**attrs)
        instance.clean()
        return attrs

    def create(self, validated_data):
        '''
        add new article
        :param validated_data:
        :return:
        '''
        return Article.objects.create(**validated_data)

    def update(self, instance, validated_data):
        '''
        修改文章内容
        :param instance:
        :param validated_data:
        :return:
        '''
        instance.title = validated_data.get('title', instance.title)
        instance.body = validated_data.get('body', instance.body)
        instance.save()
        return instance

    def get_star_dis_num(self, obj):
        try:
            return obj.stars.count()
        except:
            return '0'

    def get_star_people_list(self, obj):
        try:
            mark_list = StarsStatistics.objects.filter(star_to=obj)
            people_list = [item.mark_by.userrole.username_cn for item in mark_list]
            return people_list
        except:
            return []
