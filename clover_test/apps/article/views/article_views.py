'''
author: 鲁成
create_tiem: 2017-09-13
last_mod_time : 2017-09-14
'''


import datetime
import copy

from django.shortcuts import get_object_or_404

from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response

from apps.article.models import Article
from apps.article.serializers.article_serializers import ArticleListSerializer

from django.contrib.auth.models import User

from common_utils.validate_str import validate_str


'''
按照restful规范改造的接口，还未完成。
'''

class ArticlesList(generics.ListCreateAPIView):
    '''
    get 获取文章列表，并按照点赞数量倒序排序。
    post 新增文章
    '''
    queryset = Article.objects.all().order_by('-stars_record')
    serializer_class = ArticleListSerializer

    def get_queryset(self):
        author_username = self.request.GET.get('author_username', 'all')
        title = self.request.GET.get('title', 'all')
        filter_parmas = {}
        if title and title != 'all':
            filter_parmas['title'] = title

        author_obj = None
        if author_username and author_username != 'all':
            try:
                author_obj = User.objects.get(username=author_username)
            except:
                author_obj = None

        if author_obj:
            filter_parmas['author'] = author_obj

        article_obj = Article.objects.filter(**filter_parmas).order_by('-stars_record')
        return article_obj

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(author=user)



class ArticleDetail(APIView):
    '''
    get 查看
    put 编辑
    delete 删除
    :param APIView:
    :return:
    '''
    def get_article(self, article_id):
        article_obj = get_object_or_404(Article, id=article_id)
        return article_obj

    def get(self, request, pk, format=None):
        article_obj = self.get_article(article_id=pk)
        article_serializer = ArticleListSerializer(article_obj)
        return Response(article_serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        article_obj = self.get_article(article_id=pk)
        # 判断当前用户是否有权限编辑该文章
        if not request.user == article_obj.author:
            # add log
            return Response(status=status.HTTP_403_FORBIDDEN)
        serializer = ArticleListSerializer(article_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            # add log
            return Response(serializer.data)
        # add log
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        article_obj = self.get_article(article_id=pk)
        # 判断当前用户是否有权限删除该文章
        if not request.user == article_obj.author:
            # add log
            return Response(status=status.HTTP_403_FORBIDDEN)
        article_obj.delete()
        # add log
        return Response(status=status.HTTP_204_NO_CONTENT)



'''
不太符合restful规范的接口，但是功能可以正常运行
'''
# class AllArticle(APIView):
#     '''
#     get 所有文章
#     post 删除文章
#     '''
#     def get(self, request, format=None):
#         author_username = request.GET.get('author_username', 'all')
#         title = request.GET.get('title', 'all')
#         filter_parmas = {}
#         if title and title != 'all':
#             filter_parmas['title'] = title
#         author_obj = None
#         if author_username and author_username != 'all':
#             try:
#                 author_obj = User.objects.get(username=author_username)
#             except:
#                 author_obj = None
#
#         if author_obj:
#             filter_parmas['author'] = author_obj
#
#         article_obj = Article.objects.filter(**filter_parmas).order_by('-stars_record')
#         article_serializer = ArticleListSerializer(article_obj, many=True)
#         return Response(article_serializer.data, status=status.HTTP_200_OK)
#
#     def post(self, request, format=None):
#         user = request.user
#         article_id = request.POST.get('article_id')
#         article_obj = get_object_or_404(Article, id=article_id)
#
#         # 判断用户是否有权限删除
#         if not article_obj.author == user:
#             # 记录log
#             return Response({'msg': '无权删除当前文章！'}, status=status.HTTP_403_FORBIDDEN)
#
#         # 关联删除StarsStatistics数据
#         try:
#             article_obj.delete()
#             # 记录log
#             return Response({'msg': '文章删除成功！'}, status=status.HTTP_200_OK)
#         except:
#             # 记录log
#             return Response({'msg': '文章删除失败！'}, status=status.HTTP_400_BAD_REQUEST)
#
#
# class ArticleList(APIView):
#     '''
#     自己的文章
#     get 文章列表及筛选
#     post 新增文章
#     '''
#     def get(self, request, format=None):
#         '''
#         获取自己的文章
#         :param request:
#         :param format:
#         :return:
#         '''
#         # user = request.user
#         user = User.objects.get(username='lucheng')
#         title = request.GET.get('title', 'all')
#         filter_parmas = {
#             'title': title,
#         }
#
#         # 剔除空或者all的筛选条件
#         for k, v in filter_parmas.copy().items():
#             if v == '' or v == 'all':
#                 filter_parmas.pop(k)
#         print(filter_parmas)
#         article_obj = user.articles.filter(**filter_parmas).order_by('-stars_record')
#         article_serializer = ArticleListSerializer(article_obj, many=True)
#         return Response(article_serializer.data, status=status.HTTP_200_OK)
#
#     def post(self, request, format=None):
#         '''
#         发表文章
#         :param request:
#         :param format:
#         :return:
#         '''
#         user = request.user
#         title = request.POST.get('title', '')
#         body = request.POST.get('body', '')
#         add_parmas = {
#             'title': title,
#             'body': body,
#             'author': user,
#             'stars_record': 0,
#         }
#
#         # 判断特殊字符,并除去空数据
#         for k, v in add_parmas.copy().items():
#             if not validate_str(v):
#                 return Response({'msg': '含有特殊字符！', 'errorCode': '400'}, status=status.HTTP_400_BAD_REQUEST)
#             if v == '':
#                 add_parmas.pop(k)
#
#         cached_article = Article(**add_parmas)
#         try:
#             cached_article.save()
#             # 记录log
#             article_serializer = ArticleListSerializer(cached_article)
#             return Response(article_serializer.data, status=status.HTTP_200_OK)
#         except:
#             # 记录log
#             return Response({'msg': '发表文章失败！'}, status=status.HTTP_400_BAD_REQUEST)
#
#
# class ModArticle(APIView):
#     '''
#     get 获取某文章内容
#     post 修改文章内容
#     '''
#     def get_article(self, article_id):
#         article_obj = get_object_or_404(Article, id=article_id)
#         return article_obj
#
#     def get(self, request, format=None):
#         article_id = request.GET.get('article_id')
#         article_obj = self.get_article(article_id=article_id)
#         article_serializer = ArticleListSerializer(article_obj)
#         return Response(article_serializer.data, status=status.HTTP_200_OK)
#
#     def post(self, request, format=None):
#         user = request.user
#         article_id = request.POST.get('article_id')
#         article_obj = self.get_article(article_id=article_id)
#         title = request.POST.get('title', '')
#         body = request.POST.get('body', '')
#
#         # 判断当前用户是否有权限编辑文章
#         if not article_obj.author == user:
#             # 记录log
#             return Response({'msg': '无权删除当前文章！'}, status=status.HTTP_403_FORBIDDEN)
#
#         # 判断特殊字符,并除去空数据
#         if not (validate_str(title) and validate_str(body)):
#             return Response({'msg': '含有特殊字符！', 'errorCode': '400'}, status=status.HTTP_400_BAD_REQUEST)
#         if title and title != '':
#             article_obj.title = title
#         if body and body != '':
#             article_obj.body = body
#
#         try:
#             article_obj.save()
#             # 记录log
#             article_serializer = ArticleListSerializer(article_obj)
#             return Response({'msg': '文章编辑成功！', 'data': article_serializer.data}, status=status.HTTP_200_OK)
#         except:
#             # 记录log
#             return Response({'msg': '文章编辑失败！'}, status=status.HTTP_400_BAD_REQUEST)
