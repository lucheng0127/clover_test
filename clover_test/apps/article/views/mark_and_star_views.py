'''
author: 鲁成
create_tiem: 2017-09-13
'''

from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from apps.article.models import Article, StarsStatistics
from apps.article.serializers.article_serializers import ArticleListSerializer

from django.contrib.auth.models import User

from common_utils.validate_str import validate_str


class MarkArticle(APIView):
    '''
    用户给文章点赞
    '''
    def post(self, request, format=None):
        user = request.user
        article_id = request.POST.get('article_id')
        article_obj = get_object_or_404(Article, id=article_id)
        # 判断当前用户是否已经给这篇文章点赞
        star_his = StarsStatistics.objects.filter(mark_by=user, star_to=article_obj)
        if star_his:
            # 记录log
            return Response({'msg': '亲，您已经为该文章点过赞哦！', 'errorCode': '400'}, status=status.HTTP_400_BAD_REQUEST)
        # 保存点赞数据，并更新Article里面stars_record字段数据
        cache_stat_obj = StarsStatistics(mark_by=user, star_to=article_obj)
        try:
            cache_stat_obj.save()
            # 记录log
            try:
                star_num = int(article_obj.stars_num)
                article_obj.stars_record = star_num
                article_obj.save()
            except Exception as e:
                print(e)
            return Response({'msg': '点赞成功！'}, status=status.HTTP_200_OK)
        except:
            # 记录log
            return Response({'msg': '点赞失败！'}, status=status.HTTP_400_BAD_REQUEST)


# 待办，将点赞人列表返回
