#-*- coding: utf-8 -*-
'''
author: 鲁成
create_tiem: 2017-09-13
'''

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('apps.authx.urls')),
    url(r'^', include('apps.article.urls')),
]

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]
