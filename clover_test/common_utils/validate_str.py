'''
author: 鲁成
create_tiem: 2017-09-13
'''

import re

def validate_str(date_str):
    pattern = r"^[\s\S]*[\"#\$%&\'\(\)\*\+/:;<=>?@\[\\\]\^_`\{\|\}~]"
    date_str = str(date_str).replace(' ', '')
    if re.match(pattern, date_str):
        return False
    return True